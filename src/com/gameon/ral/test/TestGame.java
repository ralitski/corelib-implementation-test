package com.gameon.ral.test;

import com.gameon.libs.lobby.Arena;
import com.gameon.libs.lobby.Game;
import com.gameon.libs.lobby.Lobby;
import java.util.Random;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author ralitski
 */
public class TestGame extends Game {
    
    private BukkitRunnable runner;
    
    public TestGame(Lobby l) {
        super(TestPlugin.getInstance().getGameFactory(), l);
    }

    @Override
    public int getDefaultSize() {
        return 5;
    }

    @Override
    protected boolean startInternal(Arena arena) {
        this.runner = new BukkitRunnable() {

            @Override
            public void run() {
                this.run();
            }
        };
        runner.runTaskTimer(TestPlugin.getInstance(), 100L /* 5 sec delay */, 20L /* 1 sec ticks */);
        return true;
    }
    
    private void run() {
        for(Player p : this.getLobby().getPlayers()) {
            if((new Random()).nextInt(10) == 0) p.sendMessage("you are in map " + this.getMap().getName());
        }
    }

    @Override
    protected boolean stopInternal() {
        try {
            this.runner.cancel();
            this.runner = null;
        } catch (IllegalStateException illegalStateException) {
            //runner ain't even running
        }
        return true;
    }

    @Override
    public void leave(Player player, boolean bln) {
        String msg = player.getName() + " has left the game" + (bln ? " due to a disconnection" : "");
        for(Player p : this.getLobby().getPlayers()) {
            p.sendMessage(msg);
        }
    }

}
