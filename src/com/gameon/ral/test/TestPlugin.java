package com.gameon.ral.test;

import com.gameon.libs.GameOnPlugin;
import com.gameon.libs.lobby.GameFactory;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author ralitski
 */
public class TestPlugin extends JavaPlugin implements GameOnPlugin {
    
    private static TestPlugin instance;
    private TestGameFactory gameF;
    
    public static TestPlugin getInstance() {
        return instance;
    }
    
    @Override
    public void onEnable() {
        instance = this;
        this.gameF = new TestGameFactory();
    }
    
    @Override
    public void onDisable() {
        
    }

    @Override
    public String getGoName() {
        return ChatColor.YELLOW + "Test";
    }

    @Override
    public String getSimpleName() {
        return "test";
    }

    @Override
    public GameFactory getGameFactory() {
        return this.gameF; //TODO
    }

    @Override
    public Plugin getBukkitPlugin() {
        return this;
    }

    @Override
    public boolean isRemote() {
        return false;
    }

}
