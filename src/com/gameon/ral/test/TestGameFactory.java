package com.gameon.ral.test;

import com.gameon.libs.GameOnPlugin;
import com.gameon.libs.lobby.Arena;
import com.gameon.libs.lobby.Game;
import com.gameon.libs.lobby.GameFactory;
import com.gameon.libs.lobby.Lobby;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 *
 * @author ralitski
 */
public class TestGameFactory implements GameFactory {
    
    private List<Arena> maps;
    
    public TestGameFactory() {
        this.maps = new ArrayList<>();
        this.maps.add(new TestArena("test1", new Location(Bukkit.getWorlds().get(0), 0, 50, 0)));
        this.maps.add(new TestArena("test2", new Location(Bukkit.getWorlds().get(0), 50, 50, 0)));
    }

    @Override
    public GameOnPlugin getType() {
        return TestPlugin.getInstance();
    }

    @Override
    public Game createGame(Lobby lobby) {
        return new TestGame(lobby);
    }

    @Override
    public List<Arena> getMaps() {
        return this.maps;
    }

    @Override
    public boolean allowMapVoting() {
        return true;
    }

    @Override
    public boolean useXPtimer() {
        return true;
    }

}
