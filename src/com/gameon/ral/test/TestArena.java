package com.gameon.ral.test;

import com.gameon.libs.lobby.Arena;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author ralitski
 */
public class TestArena extends Arena {
    
    private Location loc;
    
    public TestArena(String s, Location loc) {
        super(s);
        this.loc = loc;
    }

    @Override
    public void addPlayer(Player player) {
        Material[] m = Material.values();
        Material mat = m[(new Random()).nextInt(m.length)];
        player.getInventory().addItem(new ItemStack(mat));
        player.teleport(this.loc);
    }

    @Override
    public void addModerator(Player player) {
        player.sendMessage("lele");
    }

    @Override
    public void reset() {
        for(Player p : this.getLock().getLobby().getPlayers()) {
            p.getInventory().clear();
        }
    }

    @Override
    public void removePlayer(Player player) {
    }

}
